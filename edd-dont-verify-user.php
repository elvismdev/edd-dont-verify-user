<?php
/*
Plugin Name: EDD Don't Verify User
Version: 1.0
Description: Removes the action from Easy Digital Downloads which checks if the user has an existing purchase before maded as a guest and sends him an email to activate his account if the user now create/register an account in WP after a purchase. 
Author: Elvis Morales
Author URI: http://elvismdev.io
Plugin URI: https://bitbucket.org/grantcardone/edd-dont-verify-user
Text Domain: edd-dont-verify-user
*/
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

remove_action('user_register', 'edd_add_past_purchases_to_new_user');
